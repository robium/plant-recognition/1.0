import os
import gc
import re

import cv2
import math
import numpy as np
import scipy as sp
import pandas as pd

from IPython.display import SVG
import efficientnet.tfkeras as efn
from keras.utils import plot_model
import tensorflow.keras.layers as L
from keras.utils import model_to_dot
import tensorflow.keras.backend as K
from tensorflow.keras.models import Model
#from kaggle_datasets import KaggleDatasets
from tensorflow.keras.applications import DenseNet121

import seaborn as sns
from tqdm import tqdm
import matplotlib.cm as cm
from sklearn import metrics
import matplotlib.pyplot as plt
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split


tqdm.pandas()
import plotly.express as px
import plotly.graph_objects as go
import plotly.figure_factory as ff
from plotly.subplots import make_subplots

np.random.seed(0)
tf.random.set_seed(0)

import warnings
warnings.filterwarnings("ignore")

from google.colab import drive

##########################################################


def process(img):
    return cv2.resize(img/255.0, (512, 512)).reshape(-1, 512, 512, 3)
def predict(img, model):
    return model.layers[2](model.layers[1](model.layers[0](process(img)))).numpy()[0]

def visuel_predict(url_img, model):
  fig = make_subplots(rows=1, cols=2)
  image = cv2.imread(url_img)
  test1 = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

  preds = predict(test1,model)

  colors = {"Healthy":px.colors.qualitative.Plotly[0], "Scab":px.colors.qualitative.Plotly[0], "Rust":px.colors.qualitative.Plotly[0], "Multiple diseases":px.colors.qualitative.Plotly[0]}
  if list.index(preds.tolist(), max(preds)) == 0:
      pred = "Healthy"
  if list.index(preds.tolist(), max(preds)) == 1:
      pred = "Scab"
  if list.index(preds.tolist(), max(preds)) == 2:
      pred = "Rust"
  if list.index(preds.tolist(), max(preds)) == 3:
      pred = "Multiple diseases"

  colors[pred] = px.colors.qualitative.Plotly[1]
  colors["Healthy"] = "seagreen"
  colors = [colors[val] for val in colors.keys()]
  fig.add_trace(go.Image(z=cv2.resize(test1, (205, 136))), row=1, col=1)
  fig.add_trace(go.Bar(x=["Healthy", "Multiple diseases", "Rust", "Scab"], y=preds, marker=dict(color=colors)), row=1, col=2)
  fig.update_layout(height=500, width=800, title_text="Plant Disease Predictions", showlegend=False)
  fig.update_layout(template="plotly_white")
  fig.show()


###########################################################

#if use_gdrive:
  #Connecting google drive into the google colab
  #drive.mount('/content/gdrive')
  # adresse de l'image
file_path = "/content/gdrive/My Drive/TX_Intersemestre/Plant_Disease_Kaggle/test_images/test_3.jpg"

 #############################################################

model_DN = tf.keras.models.load_model("/content/gdrive/My Drive/TX_Intersemestre/Plant_Disease_Kaggle/model_weights/model_DN.h5")
model_DN.summary()

#model_EN = tf.keras.models.load_model("/content/gdrive/My Drive/TX_Intersemestre/Plant_Disease_Kaggle/model_weights/model_EN.h5")
#model_EN.summary()

#model_EN_NS = tf.keras.models.load_model("/content/gdrive/My Drive/TX_Intersemestre/Plant_Disease_Kaggle/model_weights/model_EN_NS.h5")
#model_EN_NS.summary()

visuel_predict(file_path, model_DN)
#visuel_predict(file_path, model_EN)
#visuel_predict(file_path, model_EN_NS)
