import requests
import json
from pprint import pprint

API_KEY = "2b10Fuz5zqdXWCxbotD61fCmwe"  # Set you API_KEY here
api_endpoint = f"https://my-api.plantnet.org/v2/identify/all?api-key={API_KEY}"


image_path_1 = "/home/lewis/Documents/1Lien_vers_Documents/1Travaux/UTC/GI02/TX/Classifiers-20210301T174156Z-001/plant_organs_yolo/10.jpg"
image_data_1 = open(image_path_1, 'rb')

#image_path_2 = "../data/image_2.jpeg"
#image_data_2 = open(image_path_2, 'rb')


data = {
    'organs': ['flower']#, 'leaf']
}

files = [
    ('images', (image_path_1, image_data_1))#,
    #('images', (image_path_2, image_data_2))
]

req = requests.Request('POST', url=api_endpoint, files=files, data=data)
prepared = req.prepare()

s = requests.Session()
response = s.send(prepared)

json_result = json.loads(response.text)

pprint(response.status_code)
pprint(json_result)

if response.status_code != 200:

    raise Exception(str(response.json()))
